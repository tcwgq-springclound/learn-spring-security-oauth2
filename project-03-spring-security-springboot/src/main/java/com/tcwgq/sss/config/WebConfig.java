package com.tcwgq.sss.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 就相当于springmvc.xml文件
 *
 * @author tcwgq
 * @since 2022/6/27 16:23
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    // 默认Url根路径跳转到/login，此url为spring security提供
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // url中的特殊字符"-"在重定向时，地址栏会产生乱码，建议不使用"-"
        registry.addViewController("/").setViewName("redirect:/toLogin");
        registry.addViewController("/toLogin").setViewName("login");
    }

}
