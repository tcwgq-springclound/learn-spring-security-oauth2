package com.tcwgq.sss.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author tcwgq
 * @since 2022/6/27 18:46
 **/
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    // 定义用户信息服务（查询用户信息）
   /* @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        // 添加用户和权限
        manager.createUser(User.withUsername("zhangsan").password("123").authorities("p1").build());
        manager.createUser(User.withUsername("lisi").password("456").authorities("p2").build());
        return manager;
    }*/

    // 密码编码器
    @Bean
    public PasswordEncoder passwordEncoder() {
        // return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }

    // 安全拦截机制（最重要）
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         * spring security为防止CSRF（Cross-site request forgery跨站请求伪造）的发生，
         * 限制了除了get以外的大多数方法。
         */
        http.csrf().disable()
                .sessionManagement()
                // url中的特殊字符"-"在重定向时，地址栏会产生乱码，建议不使用"-"
                // session失效后跳转到登陆页面，并提示错误
                // .invalidSessionUrl("/toLogin?error=INVALID_SESSION")
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED) // session策略

                .and()
                .authorizeRequests()
                // .antMatchers("/r/r1").hasRole("role_1")
                // .antMatchers("/r/r1").hasAnyAuthority("p1", "p3")
                .antMatchers("/r/r1").hasAuthority("p1") // web资源授权
                .antMatchers("/r/r2").hasAuthority("p2") // web资源授权
                .antMatchers("/r/**").authenticated()// 所有/r/**的请求必须认证通过
                // .antMatchers("/r/r3").access("hasAuthority('p1') and hasAuthority('p3')")
                .anyRequest().permitAll()// 除了/r/**，其它的请求可以访问

                .and()
                .formLogin()// 允许表单登录
                .loginPage("/toLogin")// 登录页面
                .loginProcessingUrl("/login")
                .successForwardUrl("/login-success")// 自定义登录成功的页面地址
                .failureForwardUrl("/login-fail")

                .and()
                .logout()
                // 使用spring security默认的
                .logoutUrl("/logout")
                // url中的特殊字符"-"在重定向时，地址栏会产生乱码，建议不使用"-"
                .logoutSuccessUrl("/toLogin?logout");
    }

}
