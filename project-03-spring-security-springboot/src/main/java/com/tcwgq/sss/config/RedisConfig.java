package com.tcwgq.sss.config;

import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * TODO 解决spring session存入redis乱码的问题
 * TODO 目前使用GenericJackson2JsonRedisSerializer报错
 * 使用 Jackson2JsonRedisSerializer还是会报错
 * <p>
 * 注意：bean的名称必须为springSessionDefaultRedisSerializer
 */
@Configuration
public class RedisConfig extends CachingConfigurerSupport {
    // @Bean("springSessionDefaultRedisSerializer")
    public RedisSerializer<Object> redisSerializer() {
        return new Jackson2JsonRedisSerializer<>(Object.class);
    }

}