package com.tcwgq.sss.service.impl;

import com.tcwgq.sss.bean.TUser;
import com.tcwgq.sss.dao.TUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class SpringDataUserDetailsService implements UserDetailsService {
    @Resource
    private TUserMapper tUserMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 登录账号
        log.info("username={}", username);
        TUser tUser = tUserMapper.selectByUsername(username);
        if (tUser == null) {
            return null;
        }
        List<String> permissions = tUserMapper.selectPermissionsByUserId(tUser.getId());
        // 根据账号去数据库查询
        return User.withUsername(tUser.getUsername()).password(tUser.getPassword()).authorities(permissions.toArray(new String[0])).build();
    }

}