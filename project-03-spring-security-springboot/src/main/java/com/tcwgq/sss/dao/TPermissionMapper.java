package com.tcwgq.sss.dao;

import com.tcwgq.sss.bean.TPermission;
import tk.mybatis.mapper.common.Mapper;

/**
 * TPermissionDao
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
public interface TPermissionMapper extends Mapper<TPermission> {

}
