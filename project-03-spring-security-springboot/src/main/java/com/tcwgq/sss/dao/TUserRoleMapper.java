package com.tcwgq.sss.dao;

import com.tcwgq.sss.bean.TUserRole;
import tk.mybatis.mapper.common.Mapper;

/**
 * TUserRoleDao
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
public interface TUserRoleMapper extends Mapper<TUserRole> {

}
