package com.tcwgq.sss.dao;

import com.tcwgq.sss.bean.TRolePermission;
import tk.mybatis.mapper.common.Mapper;

/**
 * TRolePermissionDao
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
public interface TRolePermissionMapper extends Mapper<TRolePermission> {

}
