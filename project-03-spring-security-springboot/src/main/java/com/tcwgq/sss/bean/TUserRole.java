package com.tcwgq.sss.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * TUserRoleBean
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
@Data
@Table(name = "t_user_role")
public class TUserRole implements Serializable {
    @Id
    @Column(name = "user_id")
    private String userId;//

    @Column(name = "role_id")
    private String roleId;//

    @Column(name = "create_time")
    private Date createTime;//

    @Column(name = "creator")
    private String creator;//

}
