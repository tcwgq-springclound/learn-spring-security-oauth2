package com.tcwgq.sss.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * TUserBean
 *
 * @author tcwgq
 * @since 2022/06/28 10:46
 */
@Data
@Table(name = "t_user")
public class TUser implements Serializable {
    @Id
    @Column(name = "id")
    private Long id;// 用户id

    @Column(name = "username")
    private String username;//

    @Column(name = "password")
    private String password;//

    @Column(name = "fullname")
    private String fullname;// 用户姓名

    @Column(name = "mobile")
    private String mobile;// 手机号

}
