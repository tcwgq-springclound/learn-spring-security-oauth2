package com.tcwgq.sss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import tk.mybatis.spring.annotation.MapperScan;

// 开启spring security基于方法的授权
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EnableRedisHttpSession // 开启基于Redis的session共享
@SpringBootApplication
@MapperScan("com.tcwgq.sss")
public class SpringSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityApplication.class, args);
    }

}
