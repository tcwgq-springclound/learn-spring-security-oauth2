package com.tcwgq.sss.encrypt;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * @author tcwgq
 * @since 2022/6/28 9:06
 */
public class BCryptTest {
    @Test
    public void test1() {
        // 对原始密码加密
        String hashpw = BCrypt.hashpw("123", BCrypt.gensalt());
        String hashpw1 = BCrypt.hashpw("456", BCrypt.gensalt());
        System.out.println(hashpw);
        System.out.println(hashpw1);
        // 校验原始密码和BCrypt密码是否一致
        System.out.println(BCrypt.checkpw("123", hashpw));
        System.out.println(BCrypt.checkpw("456", hashpw1));
    }

}
