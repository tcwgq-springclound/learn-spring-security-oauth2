package com.tcwgq.uaa.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * TRolePermissionBean
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
@Data
@Table(name = "t_role_permission")
public class TRolePermission implements Serializable {
    @Id
    @Column(name = "role_id")
    private String roleId;//

    @Column(name = "permission_id")
    private String permissionId;//

}
