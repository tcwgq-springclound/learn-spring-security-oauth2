package com.tcwgq.uaa.dao;

import com.tcwgq.uaa.bean.TUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * TUserDao
 *
 * @author tcwgq
 * @since 2022/06/28 10:46
 */
public interface TUserMapper extends Mapper<TUser> {
    @Select("select * from t_user where username = #{username}")
    TUser selectByUsername(@Param("username") String username);

    @Select("SELECT `code` FROM t_permission WHERE id IN(SELECT permission_id FROM t_role_permission WHERE role_id IN(SELECT role_id FROM t_user_role WHERE user_id = #{userId}))")
    List<String> selectPermissionsByUserId(@Param("userId") Long userId);

}
