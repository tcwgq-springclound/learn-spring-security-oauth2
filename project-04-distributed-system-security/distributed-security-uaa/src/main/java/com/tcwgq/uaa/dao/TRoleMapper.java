package com.tcwgq.uaa.dao;

import com.tcwgq.uaa.bean.TRole;
import tk.mybatis.mapper.common.Mapper;

/**
 * TRoleDao
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
public interface TRoleMapper extends Mapper<TRole> {

}
