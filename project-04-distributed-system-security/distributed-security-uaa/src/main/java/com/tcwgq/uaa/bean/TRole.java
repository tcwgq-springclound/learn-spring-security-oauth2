package com.tcwgq.uaa.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * TRoleBean
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
@Data
@Table(name = "t_role")
public class TRole implements Serializable {
    @Id
    @Column(name = "id")
    private String id;//

    @Column(name = "role_name")
    private String roleName;//

    @Column(name = "description")
    private String description;//

    @Column(name = "create_time")
    private Date createTime;//

    @Column(name = "update_time")
    private Date updateTime;//

    @Column(name = "status")
    private String status;//

}
