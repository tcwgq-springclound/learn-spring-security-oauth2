package com.tcwgq.uaa.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.InMemoryClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;
import java.util.Collections;

/**
 * 配置授权服务器
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private AuthorizationServerTokenServices authorizationServerTokenServices;

    // 令牌端点安全约束
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security
                // oauth/token_key是公开；tokenkey这个endpoint当使用JwtToken且使用非对称加密时，资源服务用于获取公钥而开放的，这里指这个endpoint完全公开。
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()")                    // oauth/check_token公开
                .allowFormAuthenticationForClients()                // 表单认证（申请令牌）
        ;
    }

    /**
     * 配置客户端详细信息，支持配置多个
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        JdbcClientDetailsService jdbcClientDetailsService = new JdbcClientDetailsService(dataSource);
        jdbcClientDetailsService.setPasswordEncoder(passwordEncoder);
        // 使用数据库存储客户端信息
        clients.withClientDetails(jdbcClientDetailsService);
       /* clients.inMemory()// 使用in‐memory存储
                .withClient("c1")// client_id
                .secret(passwordEncoder.encode("secret"))
                .resourceIds("res1")
                // 该client允许的授权类型authorization_code,password,refresh_token,implicit,client_credentials
                .authorizedGrantTypes("authorization_code", "password", "client_credentials", "implicit", "refresh_token")
                .scopes("all")// 允许的授权范围
                .autoApprove(true)// false时弹出确认页面
                // 加上验证回调地址
                .redirectUris("http://www.baidu.com");*/
    }

    /**
     * 令牌访问端点配置
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .authenticationManager(authenticationManager)// 认证管理器，资源所有者密码（password）授权类型的时候，必须配置
                // .authorizationCodeServices(new InMemoryAuthorizationCodeServices())// 授权码服务，设置授权码模式的授权码如何存取，暂时采用内存方式
                .authorizationCodeServices(new JdbcAuthorizationCodeServices(dataSource))// 数据库存储
                .tokenServices(authorizationServerTokenServices)// 令牌管理服务
                // 配置使用jwt令牌，使用默认配置，只需配置tokenEnhancer
                // .tokenStore(new JwtTokenStore(jwtAccessTokenConverter()))
                // .tokenStore(new JdbcTokenStore(dataSource))
                // .accessTokenConverter(jwtAccessTokenConverter())
                // 使用默认配置，只需配置这项
                .tokenEnhancer(tokenEnhancer())
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);
    }

    private TokenEnhancerChain tokenEnhancer() {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Collections.singletonList(jwtAccessTokenConverter()));
        return tokenEnhancerChain;
    }

    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey("uaa123");
        return jwtAccessTokenConverter;
    }

    /**
     * 令牌访问服务，就一个实现类，不用注册，默认已经注册一个
     */
    // @Bean
    // @Primary
    public AuthorizationServerTokenServices authorizationServerTokenServices() {
        DefaultTokenServices service = new DefaultTokenServices();
        service.setTokenStore(new InMemoryTokenStore());// 令牌存储方式
        service.setClientDetailsService(new InMemoryClientDetailsService());// 客户端信息服务
        service.setSupportRefreshToken(true);// 是否产生刷新令牌
        service.setAccessTokenValiditySeconds(7200); // 令牌默认有效期2小时
        service.setRefreshTokenValiditySeconds(259200); // 刷新令牌默认有效期3天
        return service;
    }

}