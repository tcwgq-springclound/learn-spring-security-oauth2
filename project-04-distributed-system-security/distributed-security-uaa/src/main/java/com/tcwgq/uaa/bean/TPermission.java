package com.tcwgq.uaa.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * TPermissionBean
 *
 * @author tcwgq
 * @since 2022/06/28 21:59
 */
@Data
@Table(name = "t_permission")
public class TPermission implements Serializable {
    @Id
    @Column(name = "id")
    private String id;//

    @Column(name = "code")
    private String code;// 权限标识符

    @Column(name = "description")
    private String description;// 描述

    @Column(name = "url")
    private String url;// 请求地址

}
