package com.tcwgq.order.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2022/6/27 17:22
 **/
@RestController
public class OrderController {
    /**
     * 测试资源1
     */
    @PreAuthorize("hasAnyAuthority('p1')")
    @GetMapping(value = "/r/r1", produces = {"text/plain;charset=UTF-8"})
    public String r1() {
        String username = getUsername();
        return username + " 访问资源1";
    }

    /**
     * 测试资源2
     */
    @GetMapping(value = "/r/r2", produces = {"text/plain;charset=UTF-8"})
    public String r2() {
        String username = getUsername();
        return username + " 访问资源2";
    }

    /**
     * 测试资源3
     */
    @PreAuthorize("hasAuthority('p2')")
    @GetMapping(value = "/r/r3", produces = {"text/plain;charset=UTF-8"})
    public String r3() {
        String username = getUsername();
        return username + " 访问资源2";
    }

    /**
     * 获取当前登录用户名
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.isAuthenticated()) {
            return null;
        }
        Object principal = authentication.getPrincipal();
        String username = null;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }

}
