package com.tcwgq.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author Administrator
 * @version 1.0
 **/
@Component
public class AuthFilter extends ZuulFilter {
    private final String TOKEN_NAME = "Authorization";

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * TODO 未授权自动跳转到登陆页
     */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String path = request.getServletPath();
        if (path.startsWith("/uaa")) {
            return null;
        }

        String token = getToken(request);
        if (StringUtils.isEmpty(token)) {
            // 不存在，未登录，则拦截
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
        }

        if (!token.toLowerCase(Locale.ROOT).startsWith("bearer ")) {
            token = "bearer " + token;
        }

        ctx.addZuulRequestHeader(TOKEN_NAME, token);

        return null;
    }

    private String getToken(HttpServletRequest request) {
        String first = request.getHeader(TOKEN_NAME);
        if (StringUtils.isNotEmpty(first)) {
            return first;
        }

        String second = request.getParameter(TOKEN_NAME);
        if (StringUtils.isNotEmpty(second)) {
            return second;
        }

        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(TOKEN_NAME)) {
                return cookie.getValue();
            }
        }

        return "";
    }

}
