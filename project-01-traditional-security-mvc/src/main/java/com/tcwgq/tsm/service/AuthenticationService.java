package com.tcwgq.tsm.service;


import com.tcwgq.tsm.model.AuthenticationRequest;
import com.tcwgq.tsm.model.UserDto;

/**
 * @author tcwgq
 * @since 2022/6/27 17:12
 **/
public interface AuthenticationService {
    /**
     * 用户认证
     *
     * @param authenticationRequest 用户认证请求，账号和密码
     * @return 认证成功的用户信息
     */
    UserDto authentication(AuthenticationRequest authenticationRequest);

}
