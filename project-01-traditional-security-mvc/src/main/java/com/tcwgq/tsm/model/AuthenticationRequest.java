package com.tcwgq.tsm.model;

import lombok.Data;

/**
 * @author tcwgq
 * @since 2022/6/27 17:10
 **/
@Data
public class AuthenticationRequest {
    // 认证请求参数，账号、密码
    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

}
